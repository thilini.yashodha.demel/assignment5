# include <stdio.h>

#define PerformanceCost 500
#define AttendeesCost 3

//Function declaration
//
/*int Attendees(int C);
int Income(int C);
int Cost(int C);
int Profit(int C);*/

int Attendees(int C)
{
    return 120-(((C-15)/5)*20);
}

int Income(int C)
{
    return Attendees(C)*C;
}

int Cost(int C)
{
    return Attendees(C)*AttendeesCost+PerformanceCost;
}

int Profit(int C)
{
    return Income(C)-Cost(C);
}

int main()
{
    int C;
    printf ("Expected Profit for ticket prices: \n\n");
    for (C=5;C<50;C+=5)
    {
        printf ("Ticket price = Rs.%d \t Profit = Rs.%d \n",C,Profit(C));
    }
    return 0;
}
